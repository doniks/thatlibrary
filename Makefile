CFLAGS+=-Werror

thatlib.a: thatlib.o
	ar -rcs $@ $^

thatlib.o: thatlib.c
	gcc ${CFLAGS} -c $<

test.o: test.c
	gcc ${CFLAGS} -c $<

test: test.o thatlib.a 
	gcc -o $@ $^

run:test
	./test

clean:
	rm -rf *.o *.a test
